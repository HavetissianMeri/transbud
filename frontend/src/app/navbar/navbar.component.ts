import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery'
import { HttpClient } from '@angular/common/http'
import { AjaxService } from '../service/ajax.service';
import { FormBuilder,FormGroup } from "@angular/forms";
import { Observable } from 'rxjs';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  adForm:FormGroup
  isLogged:Observable<any>;

  constructor(private ajaxService:AjaxService, private formBuilder:FormBuilder) { 
    // this.login()
    this.initForm()
  }

  ngOnInit() {
    this.isLogged = this.ajaxService.user;
   
    
  }



  initForm(){
    console.log('initform')
    this.adForm= this.formBuilder.group({
        username:'simplon',
        password:'user'
    })
  }

  login() 
  {
    console.log('login')
    // const value = this.adForm.value
    const username = this.adForm.value.username;
    const password = this.adForm.value.password;
    this.ajaxService.login(username, password).subscribe(
      (response) => console.log,
      (error) => console.log,
    );

    
  
  }
  logged() {
    return this.ajaxService.isLoggedIn();
  }

  logout(){

    this.ajaxService.logout();
    
  }
}
