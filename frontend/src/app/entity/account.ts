import { Operation } from "./operation";

export interface Account {

  id?:number;
  name?:string;
  banque?:string;
  solde?:number;
  operations?:Operation[];
  
}
