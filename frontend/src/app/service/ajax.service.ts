import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { Operation } from '../entity/operation';
import { Account } from '../entity/account';
import { tap } from "rxjs/operators";
import * as jwtdecode from "jwt-decode";
import { UserComponent } from '../user/user.component';

@Injectable({
  providedIn: 'root'
})
export class AjaxService {

  private urlOperation = 'http://localhost:8080/api/operation/';
  private urlAccount = 'http://localhost:8080/api/user/account/';
  private apiUrl = 'http://localhost:8080/api/login_check';
  private urlUser = 'http://localhost:8080/api/user';

  user = new BehaviorSubject<any>(null);
  
  constructor (private http:HttpClient) {
    
    if(localStorage.getItem('token')) {
      this.user.next(
        jwtdecode(localStorage.getItem('token'))
        );
    }
  }

  public getAllOperation():Observable<Operation[]>
  {
    return this.http.get<Operation[]>(this.urlOperation)
  }

  public addOneOperation(operation:Operation, id):Observable<Operation>
  {
    return this.http.post<Operation>(`http://localhost:8080/api/user/account/${id}/operation/`,operation)
  }

  public removeOneOperation(operation:Operation, idAccount:number):Observable<any>
  {
  
    return this.http.delete(`http://localhost:8080/api/user/account/${idAccount}/operation/${operation.id}`)
  }

  public getAllAccount():Observable<Account[]>
  {
    return this.http.get<Account[]>(this.urlAccount);
  }

  public getOneAccount(id:number):Observable<Account>
  {
    return this.http.get<Account>(`${this.urlAccount}${id}`);
  }

  public getAllOperationByAccount(id) {

    return this.http.get<Operation[]>(`http://localhost:8080/api/user/account/${id}/operation`);
    
  }


  public addUser(user:UserComponent):Observable<UserComponent>
  {
    return this.http.post<UserComponent>(`http://localhost:8080/user`,user)
  }

  

  login (username:string, password:string): Observable<any> {
    console.log('service > login')
    return this.http.post<any>(this.apiUrl, {
    username:username, 
    password:password
    }).pipe(
      
      tap(response => {
        console.log('response', response)
        if(response.token) {
          
          localStorage.setItem('token', response.token);
         
          this.user.next(jwtdecode(response.token));
        }
      })
    );
  }
 
  logout() {
    localStorage.removeItem('token');
    this.user.next(null);
  }
  
  isLoggedIn():boolean {
    if(localStorage.getItem('token')) {
      return true;
    }
    return false;
  }


}