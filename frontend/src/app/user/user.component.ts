import { Component, OnInit, } from '@angular/core';
import { User } from '../entity/user';
import { FormBuilder, FormGroup } from "@angular/forms";
import { AjaxService } from '../service/ajax.service';


@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  adForm: FormGroup;



  constructor(private formBuilder: FormBuilder, private ajaxUser: AjaxService) {
    this.ngOnInit()
  }
  ngOnInit() {
    this.adForm = this.formBuilder.group({
      surname: '',
      name: '',
      email: '',
      password: '',
    })

  }


  addUserSubmit() {

    console.log(typeof (this.adForm));
    console.log(this.adForm);
    this.ajaxUser.addUser(this.adForm.value).subscribe();

   


  }

}
