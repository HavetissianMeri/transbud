<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\CategoryRepository;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\JsonResponse;



/**
 * @Route("/category", name="category")
 */
class CategoryController extends AbstractController
{
    private $serializer;

    const NORMALIZER_FORMAT = ['attributes' => ['id', 'label', 'operation']];

    public function __construct()
    {
        $encoder = new JsonEncoder();

        $normalizer = new ObjectNormalizer();
        $normalizer->setCircularReferenceLimit(1);
        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId();
        });

        $this->serializer = new Serializer([$normalizer], [$encoder]);

    }




    /**
     * @Route("/", methods = {"GET"})
     */
    public function findAllOperation(CategoryRepository $repo)
    {

        $list = $repo->findAll();
        $data = $this->serializer->normalize($list, null, [self::NORMALIZER_FORMAT]);
        return JsonResponse::fromJsonString($this->serializer->serialize($data, "json"));
    }

}
