<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Serializer\Serializer;
use App\Entity\User;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Repository\UserRepository;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;



class UserController extends Controller

{
    private $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }


    /**
     * @Route("/user", methods={"POST"})
     */
    public function addUser(Request $req, UserRepository $user,UserPasswordEncoderInterface $encoder)
    {


        $manager = $this->getDoctrine()->getManager();

        $message = $this->serializer->deserialize(
            $req->getContent(),
            User::class,
            "json"
        );
        $message->setPassword($encoder->encodePassword($message, $message->getPassword()));
        $manager->persist($message);
        $manager->flush();

        $json = $this->serializer->serialize($message, "json");

  
        return JsonResponse::fromJsonString($json, 201);



    }



}



