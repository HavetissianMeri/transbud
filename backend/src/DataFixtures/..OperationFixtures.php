<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Operation;
use App\Entity\Account;
use App\Entity\User;

class OperationFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // $product = new Product();

        $operations = [];


        $user = new User();
        $user->setEmail('grumpy@disney.fr');
        $user->setName("Grumpy");
        $user->setPassword('123456789');
        $manager->persist($user);



        $account = new Account();
        $account->setBanque('Banque des nains du nord');
        $account->setUser($user);
        $account->setDescription("Compte pro");
        $account->setSolde(123456);

        $manager->persist($account);

        for ($i = 0; $i < 12; $i++) { 
            # code...

            $operation = new Operation();
            $operation->setMontant(-123);
            $operation->setDate(\DateTime::createFromFormat('Y-m-d', '2018-05-12'));
            $operation->setAccount($account);

            $manager->persist($operation);

        }
        // $manager->persist($product);
        $manager->flush();
    }
}
