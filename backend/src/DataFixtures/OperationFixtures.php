<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Operation;
use App\Entity\Account;

class OperationFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        

       
            $account = new Account();
            $account->setBanque("Banque nanique");
            $account->setName("Mon Compte Perso");
            $account->setSolde(1000000);

            $manager->persist($account);

        
        $manager->flush();


        $tab = [123, 456, -12345, 12, 61, -123456, 5];
        foreach ($tab as $key => $value) {
            $operation = new Operation();
            $operation->setMontant($value);


            $account->setBanque("Banque nanique");
            $account->setName("Mon Compte Perso");
            $account->setSolde(1000000);

            $operation->setAccount($account);

            $manager->persist($operation);


        }


        $manager->flush();
    }
}
