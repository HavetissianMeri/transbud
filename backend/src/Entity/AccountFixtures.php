<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Account;

class AccountFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $tab = [1, 2];

        foreach ($tab as $key => $value) {
            $account = new Account();
            $account->setBanque("Banque nanique");
            $account->setName("Mon Compte Perso");
            $account->setSolde(1000000);

            $manager->persist($account);

        }
        $manager->flush();

    }
}
